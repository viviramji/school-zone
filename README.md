# School

# TODO: 

* [x] Initialize Angular Frontend
   * [x] Actual school app
   * [x] School admin app
   * [x] Product admin app
   * [x] Sandbox app

## Terminal first command

```ng new school --skipInstall=true --minimal=true -- createApplication=false --skipGit=true --style=scss  --skipTests=true --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./```

## creating apps

- ng g application school --minimal=true --routing --style=scss --prefix=zs --skipTests=true
- ng g application school --minimal=true --routing --style=scss --prefix=zs --skipTests=true
- ng g application admin --minimal=true --routing --style=scss --prefix=zs --skipTests=true
- ng g application super --minimal=true --routing --style=scss --prefix=zs --skipTests=true
- ng g application sandbox --minimal=true --routing --style=scss --prefix=zs --skipTests=true